from django.db import models

# Create your models here.


class Prioridad(models.Model):
    descripcion = models.CharField('Descripcion', max_length=50,)

    def __str__(self):
        return str(self.id) + '-' + self.descripcion


class subir_archivos(models.Model):
    ruta_archivo = models.CharField('Ruta_archivo', max_length=100,)
    fecha_carga = models.DateTimeField('Fecha_carga')

    def __str__(self):
        return str(self.id) + '-' + self.ruta_archivo + '-' + self.fecha_carga
